import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class ArenaStorage {

    // takes a drone area object, and converts it a Json object
    JSONObject objectToJson(DroneArena d){

        JSONArray arr = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("areaX", d.getSizeX());
        jsonObject.put("areaY", d.getSizeY());

        //loops through each drone stored in drones array
        for (int i = 0; i < d.getDronesCount(); i++) {
            JSONObject droneObject = new JSONObject();
            try{
                Drone tmpDrone = d.getDrones().get(i);
                droneObject.put("id", tmpDrone.getID());
                droneObject.put("xPos", tmpDrone.getPositionX());
                droneObject.put("yPos", tmpDrone.getPositionY());
                droneObject.put("direction", tmpDrone.getDirection().toString());

                //adds drone object to drones array
                arr.add(i, droneObject);
            }catch (Exception ex){
                ex.printStackTrace();
                return null;
            }
            //adds all drones into json object
            jsonObject.put("Drones",arr);
        }
        return jsonObject;
    }

    // takes json file and converts it to DroneArea with all drones
    DroneArena JsonToObject(JSONObject jo){
        DroneArena droneArena;
        try{
            Long x = (Long) jo.get("areaX");
            Long y = (Long) jo.get("areaY");

            //Create new drone arena with x,y from json
            droneArena = new DroneArena(x.intValue(),y.intValue());

            JSONArray drones = (JSONArray) jo.get("Drones");
            //loops through each drone in json array
            for (Object drone : drones) {
                JSONObject droneObj = (JSONObject) drone;
                Long xPos = (Long) droneObj.get("xPos");
                Long yPos = (Long) droneObj.get("yPos");
                String dir = (String) droneObj.get("direction");
                Direction d = Direction.fromString(dir);

                //adds drone to arena
                droneArena.addDrone(xPos.intValue(), yPos.intValue(), d);
            }
        }catch (Exception ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        System.out.println("Loaded");
        return droneArena;
    }

    JSONObject processFile(char option, JSONObject js, String fileName){
        switch (option){
            case 'L':
                JSONParser parser = new JSONParser();
                try{
                    Object o = parser.parse(new FileReader(fileName+".txt"));
                    return (JSONObject) o;
                }catch (Exception ex){
                    ex.printStackTrace();
                    return null;
                }
            case'S':
                try{
                    FileWriter filewriter = new FileWriter(fileName+".txt");
                    filewriter.write(js.toJSONString());
                    filewriter.flush();
                    System.out.println("Saved");
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
        return null;
    }

}
