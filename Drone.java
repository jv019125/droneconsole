public class Drone {
    private int positionX;
    private int positionY;
    private final int ID;
    private static int UID;
    Direction direction;

    public int getPositionX() {
        return positionX;
    }

    public int getPositionY() {
        return positionY;
    }

    public int getID() {
        return ID;
    }

    public Direction getDirection() {
        return direction;
    }

    public Drone(int x, int y, Direction d){
        this.positionX = x;
        this.positionY = y;
        this.direction = d;
        UID++;
        this.ID = UID;
    }

    boolean isHere(int x, int y){
        return this.positionX == x && this.positionY == y;
    }

    public String toString(){
        return "Drone " + this.ID +
                " is at " + this.positionX +","+this.positionY+
                " and is facing " +this.direction;
    }

    void displayDrone(ConsoleCanvas c){
        c.showIt(this.positionX, this.positionY);
    }

    void checkToMove(DroneArena area){
        int nx = this.positionX;
        int ny = this.positionY;

        //puts the drone in next position
        if(this.direction == Direction.North){
            ny++;
        }else if(this.direction == Direction.East){
            nx++;
        }else if(this.direction == Direction.South){
            ny--;
        }else if(this.direction == Direction.West){
            nx--;
        }

        //looks to see if drone can be moved here
        if(area.checkToMove(nx, ny)){
            //moves drone to new position
            this.positionX = nx;
            this.positionY = ny;
        }else{
            //if drone cant be moved, turn the direction round one
            this.direction = direction.next(this.direction);
        }
    }
}
