public class ConsoleCanvas {

    private final int row;
    private final int col ;
    char [][] box;

    public ConsoleCanvas(int row, int col) {
        this.row = row;
        this.col = col;
        box = new char[row][col];
        for (int i = 0; i < row; i++) { //Rows
            for (int j = 0; j < col; j++) { //Columns
                if (i == 0 || i == row-1 || j == 0 || j == col-1){
                    box[i][j] = '#';
                } else{
                    box[i][j] = ' ';
                }
            }
        }
    }

    public static void main(String[] args) {
        ConsoleCanvas c = new ConsoleCanvas(100, 20);
        c.showIt(4,3);
        System.out.println(c.toString());
    }

    void showIt(int x, int y) {
        if (x != 0 || x != row-1 || y != 0 || y != col-1){
            box[x][y] = 'd';
        }
    }

    public String toString() {
        StringBuilder map = new StringBuilder();
        for (int i = 0; i <row ; i++) {
            for (int j = 0; j <col ; j++) {
                map.append(box[i][j]);
            }
            map.append("\n");
        }
        return map.toString();
    }
}
