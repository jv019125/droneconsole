import java.util.Random;

public enum Direction {
    North,
    South,
    East,
    West;

    //selects a random direction from Direction values
    public static Direction getRandom(){
        Random r = new Random();
        return values()[r.nextInt(values().length)];
    }

    //moves clockwise to next direction
    public Direction next(Direction direction) {
        if(direction == Direction.North){
            direction = Direction.East;
        }else  if(direction == Direction.East){
            direction = Direction.South;
        }else if(direction == Direction.South){
            direction = Direction.West;
        }else {
            direction = Direction.North;
        }
        return direction;
    }


    //converts string to Direction
    public static Direction fromString(String s){
        s = s.trim().toLowerCase();
        switch (s) {
            case "north":
                return Direction.North;
            case "east":
                return Direction.East;
            case "south":
                return Direction.South;
            case "west":
                return Direction.West;
            default:
                return null;
        }
    }
}
