import org.json.simple.JSONObject;

import java.util.Scanner;

class DroneInterface {

    private DroneArena myArena;				// arena in which drones are shown
    private final ArenaStorage store;

    public DroneInterface() {
        Scanner s = new Scanner(System.in);
        myArena = new DroneArena(10, 20);	// create arena of size 20*6
        store = new ArenaStorage();

        char option;
        do {
            System.out.print("Enter:\nA: Add drone,\nI: get Information,\nD: Display Drones,\nM: Move drones 10 times,\nB: Build new arena,\nS: Save arena,\nL: Load arena,\nX: exit.\n");
            option = s.next().charAt(0);
            s.nextLine();
            if (option >= 'a' && option <= 'z') {
                option -= 32;
            }
            switch (option) {
                case 'A' :
                    myArena.addDrone();
                    break;
                case 'I' :
                    System.out.print(myArena.toString());
                    break;
                case 'D':
                    this.DisplayDrones();
                    break;
                case 'M':
                    try {
                        multiMoves();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    break;
                case 'B':
                    NewABuilding();
                    break;
                case 'S':
                case 'L':
                    processArena(option);
                    break;
                case 'X':
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + option);
            }
        } while (option != 'X');
        s.close();
    }

    void processArena(char op) {
        Scanner sc = new Scanner(System.in);
        String params;
        switch (op) {
            case 'L':
                System.out.print("Enter the file name for the arena: ");
                params = sc.nextLine();
                JSONObject oj = store.processFile(op, null, params);
                myArena = store.JsonToObject(oj);
                break;
            case 'S':
                try {
                    System.out.print("Enter the file name for the arena: ");
                    params = sc.nextLine();
                    JSONObject jsonObject = store.objectToJson(myArena);
                    if (jsonObject != null) {
                        store.processFile(op, jsonObject, params.trim());
                    } else {
                        throw new Exception("Failed to store arena");
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
        }
    }

    void multiMoves() throws InterruptedException {
        for (int i = 0; i < 10; i++) {
            myArena.moveAllDrones();
            DisplayDrones();
            Thread.sleep(200,1);
        }
    }

    void DisplayDrones() {
        ConsoleCanvas c = new ConsoleCanvas(myArena.getSizeX(), myArena.getSizeY());
        myArena.showDrones(c);
        System.out.println(c.toString());
    }

    public void NewABuilding(){
        int x,y;
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter x,y for new building size (x,y): ");
        String params = sc.nextLine();

        try{
            String[] p1 = params.split(",");
            x = Integer.parseInt(p1[0].trim());
            y = Integer.parseInt(p1[1].trim());
            if (x < 3 || y < 3) {
                throw new Exception("Needs to be 3,3 or larger");
            }
            this.myArena = new DroneArena(x,y);
        } catch (Exception ex){
            System.out.println("value entered incorrectly, creating default size");
            this.myArena = new DroneArena();
        }
    }

    public static void main(String[] args) {
        new DroneInterface();
    }

}
