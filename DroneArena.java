import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DroneArena {

    private int sizeX, sizeY;

    private final List<Drone> drones = new ArrayList<>();

    public List<Drone> getDrones() {
        return drones;
    }

    int getSizeX() {
        return sizeX;
    }

    int getSizeY() {
        return sizeY;
    }

    int getDronesCount() {
        return drones.size();
    }

    int maxPossibleDrones() {
        return (getSizeX()-2) * (getSizeY()-2);
    }

    boolean checkToAddDrone() {
        return getDronesCount() < maxPossibleDrones();
    }

    public DroneArena(){
       new DroneArena(10,20);
    }

    public DroneArena(int x, int y){
        this.sizeX = x;
        this.sizeY = y;
    }

    void addDrone(){
        Random r1 = new Random();
        int xPosition = r1.nextInt(this.sizeX)+1;
        int yPosition = r1.nextInt(this.sizeY)+1;

        addDrone(xPosition, yPosition ,Direction.getRandom());
    }

    void addDrone(int x, int y, Direction d){
        if(getDronePosition(x, y) == null && x < this.sizeX-1 && y < this.sizeY-1){
            drones.add(new Drone(x, y, d));
        }else{
            try{
                if(checkToAddDrone()){
                    addDrone();
                }else{
                    throw new Exception("Arena is full!");
                }
            }catch (Exception ex){
                System.out.println(ex.getMessage());
            }
        }
    }

    public String toString(){
        StringBuilder stringBuilder;
        stringBuilder = new StringBuilder("The drone area is " + this.sizeX + "," + this.sizeY);
        for (Drone drone : drones) {
            stringBuilder.append("\n\t").append(drone.toString());
        }
        stringBuilder.append("\n");

        return stringBuilder.toString();
    }

    private Drone getDronePosition(int x, int y){
        for (Drone n:drones) {
            if(n.isHere(x,y)){
                return n;
            }
        }
        return null;
    }

    void showDrones(ConsoleCanvas c){
        for (Drone d:drones) {
            d.displayDrone(c);
        }
    }

    boolean checkToMove(int x, int y){
        return x < this.sizeX - 1 && y < this.sizeY - 1
                && x != 0 && y != 0
                && getDronePosition(x, y) == null;
    }

    void moveAllDrones(){
        for (Drone d:drones) {
           d.checkToMove(this);
        }
    }

}
